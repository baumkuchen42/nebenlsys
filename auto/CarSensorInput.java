/** Schnittstelle f�r Eingabeeinheiten im Projekt zu Wissensverabrbeitung WS17/18
* Die Schnittstelle darf nur in Absprache mit dem Dozenten ver�ndert werden.
*/
interface CarSensorInput{
	/** es gibt vier Sensoren: VL ist vorne links am Auto angebracht, VR vorne rechts, 
	* HL hinten links und HR hinten rechts.
	*/
	enum Sensor {VL,VR,HL,HR;}
	
	/** Liefert die aktuell gemessene Entfernung zum naechsten ortbaren Objekt des 
	* Sensors s. Die Ausf�hrung kann bei der Realisierung mit dem NXTUltrasonicSensor
	* einige Zeit in Anspruch nehmen (vgl. Doku von Lejos)
	* @param s der Sensor, der abgefragt werden soll
	* @return die aktuell gemessene Entfernung zum naechsten ortbaren Objekt in cm???
	* @throws CarException, wenn kein g�ltiger Wert gelesen werden konnte
	*/
	double getDistance(Sensor s) throws CarException;
	
}