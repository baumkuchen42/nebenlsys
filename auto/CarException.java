/** Exception Klasse f�r das Projekt in Wissensverarbeitung im WS17/18. 
* Sie k�nnen Spezialisierungen f�r verschiedene Fehlertypen realisieren
* oder die das Problem einfach direkt in dieser Exception kodieren. Sie d�rfen die 
* Klasse nat�rlich beleibig anpassen.
*/
class CarException extends Exception{
	CarException(){ super();}
	CarException(String s){super(s);}
	CarException(Exception e){super(e);}
}