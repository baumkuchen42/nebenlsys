
import lejos.hardware.motor.NXTRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;

public class EV3Test {
	private RegulatedMotor leftMotor;
	private RegulatedMotor rightMotor;
	private RegulatedMotor steeringMotor;
	
	public static void main(String[] args) {
		new EV3Test();
	}
	
	public EV3Test() {
		leftMotor = new NXTRegulatedMotor(MotorPort.A);
		rightMotor = new NXTRegulatedMotor(MotorPort.C);
		steeringMotor = new NXTRegulatedMotor(MotorPort.B);
		leftMotor.setSpeed(100);
		rightMotor.setSpeed(100);
		leftMotor.backward();
		rightMotor.backward();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		leftMotor.stop();
		rightMotor.stop();
	}
}
