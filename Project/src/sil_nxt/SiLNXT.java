package sil_nxt;

import base.CarController;
import base.ConsoleOutput;
import nxt.NXT;
import sil.SiLTest;

public class SiLNXT {
	public static void main(String[] args) {
		CarController ctrl = new CarController(new SiLTest(), new NXT(), new ConsoleOutput());
		ctrl.main();
	}
}
