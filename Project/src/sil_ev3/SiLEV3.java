package sil_ev3;

import ev3.EV3;
import sil.SiLTest;
import base.CarController;
import base.ConsoleOutput;

public class SiLEV3 {
	public static void main(String[] args) {
		CarController ctrl = new CarController(new SiLTest(), new EV3(), new ConsoleOutput());
		ctrl.main();
	}
}
