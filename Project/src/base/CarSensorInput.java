package base;

/** Schnittstelle für Eingabeeinheiten im Projekt zu Wissensverabrbeitung WS17/18
* Die Schnittstelle darf nur in Absprache mit dem Dozenten verändert werden.
*/
public interface CarSensorInput{
	public enum Sensor {
		FRONTLEFT, //1 @ EV3
		FRONTRIGHT, //4 @ EV3
		BACKLEFT,  //3  @ EV3
		BACKRIGHT;  //2 @ EV3
	}
	
	/** Liefert die aktuell gemessene Entfernung zum naechsten ortbaren Objekt des 
	* Sensors s. Die Ausführung kann bei der Realisierung mit dem NXTUltrasonicSensor
	* einige Zeit in Anspruch nehmen (vgl. Doku von Lejos)
	* @param s der Sensor, der abgefragt werden soll
	* @return die aktuell gemessene Entfernung zum naechsten ortbaren Objekt in cm???
	* @throws CarException, wenn kein gültiger Wert gelesen werden konnte
	*/
	public double getDistance(Sensor s) throws CarException;
	
}