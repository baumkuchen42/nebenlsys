package base;

/** Exception Klasse für das Projekt in Wissensverarbeitung im WS17/18. 
* Sie können Spezialisierungen für verschiedene Fehlertypen realisieren
* oder die das Problem einfach direkt in dieser Exception kodieren. Sie dürfen die 
* Klasse natürlich beliebig anpassen.
*/
public class CarException extends Exception{
	public CarException(){ 
		super();
	}
	
	public CarException(String s){
		super(s);
	}
	
	public CarException(Exception e){
		super(e);
	}
}
