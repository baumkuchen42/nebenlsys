package base;

import base.CarSensorInput.Sensor;

/* the car */
public class CarController {
	private CarSensorInput sensorInputListener;
	private CarMotorOutput motor;
	private boolean running = true;
	private UserNotifyOutput notifyOutput;
	private final int pollingTime = 5;
	private final int waitingTime = 25;
	
	private static final int MAX_SPEED = 100; // percentage of max speed of motor output
	private static final int MANEUVER_SPEED = 20;
	private static final double MIN_DIST = 20; //minimal distance to closest object, car should be able to turn around in this distance
	private static final double WARN_DIST = MIN_DIST + 25;
	private static final double MAX_MANEUVER_TIME = 3000; // in millisecounds
	
	private double distanceFrontLeft = -1;
	private double distanceFrontRight = -1;
	private double distanceBackLeft = -1;
	private double distanceBackRight = -1;
	private State currentState = State.STANDING;
	
	//states of the car
	public enum State {
		STANDING,
		FORWARDS,
		BACKWARDS,
		DECCELERATING,
		LEFT,
		RIGHT
		;
	}
	
	/*
	 * asynchronous query of sensory input (via 4 threads for each sensor) and reacting on values 
	 * (e.g. stop when value, which means distance to next object, is 0)
	 * also print values in terminal if they've changed
	 */
	public CarController(CarSensorInput sensorInputListener, CarMotorOutput motor, UserNotifyOutput notifyOutput) {
		this.sensorInputListener = sensorInputListener;
		this.motor = motor;
		this.notifyOutput = notifyOutput;
	}
	
	public void main() {
		try {
			if(isClear(Sensor.FRONTLEFT) && isClear(Sensor.FRONTRIGHT))
				setState(State.FORWARDS);
		} catch (CarException e) {
			handleCarException(e);
		}
		//start threads for monitoring sensory input and reacting to it
		new Thread(new Runnable() {
			public void run() {
				while(running){
					checkSensors();
				}
			}
		}).start();
	}
	
	public void setState(State state) throws CarException {
		this.currentState = state;
		double startingTime = System.currentTimeMillis();
		//System.out.println(this.currentState.name());
		switch(currentState) {
		case FORWARDS:
			driveForwards();
			return;
		case LEFT:
			while(!isClear(Sensor.FRONTRIGHT)) {
				this.motor.steering(-100);
				if(System.currentTimeMillis() - startingTime >= MAX_MANEUVER_TIME) {
					setState(State.BACKWARDS);
				}
				try {
					Thread.sleep(waitingTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			break;
		case RIGHT:
			while(!isClear(Sensor.FRONTLEFT)) {
				this.motor.steering(100);
				if(System.currentTimeMillis() - startingTime >= MAX_MANEUVER_TIME) {
					setState(State.BACKWARDS);
				}
				try {
					Thread.sleep(waitingTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			break;
		case DECCELERATING:
			int speed = MAX_SPEED;
			while(!isClear(Sensor.FRONTLEFT) || !isClear(Sensor.FRONTRIGHT)) {
				speed -= 0.05*MAX_SPEED;
				this.motor.setSpeed(speed);
				if(speed == 0) {
					setState(State.BACKWARDS);
				}
				try {
					Thread.sleep(waitingTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			break;
		case BACKWARDS:
			driveBackwards();
			try {
				Thread.sleep(waitingTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
		} 
		if(this.currentState != State.FORWARDS && isClear(Sensor.FRONTLEFT) && isClear(Sensor.FRONTRIGHT)) // if it got out of its while-loop, it's ready to drive forward again
			setState(State.FORWARDS);
	}
	
	void checkSensors() {
		checkSensorFrontRight();
		try {
			Thread.sleep(pollingTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		checkSensorFrontLeft();
		//checkSensorBackLeft();
		//checkSensorBackRight();
		try {
			Thread.sleep(pollingTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void checkSensorFrontLeft() {
		try {
			double new_distance = sensorInputListener.getDistance(Sensor.FRONTLEFT);
			if(new_distance < MIN_DIST /*|| Double.valueOf(new_distance).isInfinite()*/) {
				setState(State.DECCELERATING);
			}
			else if(new_distance < WARN_DIST) {
				setState(State.RIGHT);
			}
			else if(currentState != State.FORWARDS) {
				setState(State.FORWARDS);
			}
			if(new_distance != distanceFrontLeft) {
				refresh_value(Sensor.FRONTLEFT, new_distance);
				distanceFrontLeft = new_distance;
			}
		}
		catch(CarException e) {
			handleCarException(e);
		}
	}
	
	private void checkSensorFrontRight() {
		try {
			double new_distance = sensorInputListener.getDistance(Sensor.FRONTRIGHT);
			if(new_distance < MIN_DIST /*|| Double.valueOf(new_distance).isInfinite()*/) {
				setState(State.DECCELERATING);
			}
			else if(new_distance < WARN_DIST) {
				setState(State.LEFT);
			}
			else if(currentState != State.FORWARDS) {
				setState(State.FORWARDS);
			}
			if(new_distance != distanceFrontRight) {
				refresh_value(Sensor.FRONTRIGHT, new_distance);
				distanceFrontRight = new_distance;
			}
		}
		catch(CarException e) {
			handleCarException(e);
		}
	}
	
	private void checkSensorBackLeft() {
		try {
			double new_distance = sensorInputListener.getDistance(Sensor.BACKLEFT);
			if(this.currentState == State.BACKWARDS) {
				setState(State.RIGHT);
			}
			if(new_distance != distanceBackLeft) {
				refresh_value(Sensor.BACKLEFT, new_distance);
				distanceBackLeft = new_distance;
			}
		}
		catch(CarException e) {
			handleCarException(e);
		}
	}
	
	private void checkSensorBackRight() {
		try {
			double new_distance = sensorInputListener.getDistance(Sensor.BACKRIGHT);
			if(this.currentState == State.BACKWARDS) {
				setState(State.LEFT);
			}
			if(new_distance != distanceBackRight) {
				refresh_value(Sensor.BACKRIGHT, new_distance);
				distanceBackRight = new_distance;
			}
		}
		catch(CarException e) {
			handleCarException(e);
		}
	}

	private void refresh_value(Sensor sensor, double value) {
		notifyOutput.refresh_value(sensor, value);
	}
	
	private void handleCarException(CarException e) {
		if(e instanceof NoDataException) {
			showNoDataWarning();
		}
		else if(e instanceof NoReactionException) {
			initiateNoReactionEmergency();
		}
		else {
			// what if type could not be determined
		}
	}
	
	public synchronized void driveForwards() throws NoReactionException {
		try {
			motor.steering(0);
			motor.setSpeed(MAX_SPEED);
		} catch (CarException e) {
			throw new NoReactionException();
		}
	}
	
	public synchronized void driveBackwards() throws NoReactionException {
		try {
			motor.steering(0);
			motor.setSpeed(-MANEUVER_SPEED);
		} catch (CarException e) {
			throw new NoReactionException();
		}
	}
	
	private boolean isClear(Sensor sensor) {
		try {
			return sensorInputListener.getDistance(sensor) > WARN_DIST;
		} 
		catch (CarException e) {
			showNoDataWarning();
			return false;
		}
	}
	
	private void showNoDataWarning() {
		notifyOutput.showNoDataWarning();
	}
	
	private void initiateNoReactionEmergency() {
		notifyOutput.showNoReactionWarning();
	}
	
	public void stopProgram() {
		System.out.println("Program terminated");
		this.running = false;
		try {
			motor.steering(0);
			motor.setSpeed(0);
		} catch (CarException e) {
			e.printStackTrace();
		}
	}
}
