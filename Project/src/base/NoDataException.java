package base;

public class NoDataException extends CarException {
	public NoDataException(){ 
		super();
	}
	
	public NoDataException(String s){
		super(s);
	}
	
	public NoDataException(Exception e){
		super(e);
	}
}
