package base;

import base.CarSensorInput.Sensor;

public class ConsoleOutput implements UserNotifyOutput{

	@Override
	public void showNoDataWarning() {
		System.out.println("no value");
	}

	@Override
	public void showNoReactionWarning() {
		System.out.println("CAUTION! CAR DID NOT REACT TO COMMAND!");
	}

	@Override
	public void refresh_value(Sensor sensor, double value) {
		System.out.println(sensor.name() + " : " + value);
	}

}
