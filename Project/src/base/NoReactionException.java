package base;

public class NoReactionException extends CarException {
	public NoReactionException(){ 
		super();
	}
	
	public NoReactionException(String s){
		super(s);
	}
	
	public NoReactionException(Exception e){
		super(e);
	}
}