package base;

public interface UserNotifyOutput {
	public void showNoDataWarning();
	public void showNoReactionWarning();
	public void refresh_value(base.CarSensorInput.Sensor sensor, double value);
}
