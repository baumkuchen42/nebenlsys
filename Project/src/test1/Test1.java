package test1;

import base.CarController;
import base.CarException;
import base.CarMotorOutput;
import base.CarSensorInput;
import base.ConsoleOutput;
import base.NoDataException;
import base.NoReactionException;

/*
 * simulates environment and tests if CarController reacts correctly to it
 */

public class Test1 implements CarMotorOutput, CarSensorInput {
	private int speed;
	private int steeringDegree;
	private double distanceFrontLeft = 2;
	private double distanceFrontRight = 13;
	private double distanceBackLeft = 42;
	private double distanceBackRight = 26;
	
	public static void main(String[] args) {
		new Test1().main();
	}
	
	void main() {
		CarController ctrl = new CarController(this, this, new ConsoleOutput());
		ctrl.main();
		
		// unit tests
		System.out.println("\n-----unit tests--------");
		testDriveForwards();
		testCollisionAvoidanceBaseMethod();
		
		// system tests
		System.out.println("\n-----system tests--------");
		testCollisionAvoidanceFront();
		testCollisionAvoidanceBack();
		
		ctrl.stopProgram();
	}
	
	///////////////// unit tests //////////////////////////////////////////////////////////////
	
	private void testDriveForwards() {
		Test1 test = new Test1();
		CarController ctrl = new CarController(test, test, new ConsoleOutput());
		try {
			ctrl.driveForwards();
		} catch (CarException e) {
			e.printStackTrace();
		}
		if(assertEquals(test.speed, 50) && assertEquals(test.steeringDegree, 0)) {
			System.out.println("SUCCESSFUL: Test Drive Forwards");
		}
		else {
			System.out.println("FAILED: Test Drive Forwards. Expected speed 50 and steering degree 0, got "
							 	+ test.speed + " and " + test.steeringDegree);
		}
	}
	
	private void testCollisionAvoidanceBaseMethod() {
		Test1 test = new Test1();
		CarController ctrl = new CarController(test, test, new ConsoleOutput());
		try {
			ctrl.collisionAvoidance(Sensor.FRONTRIGHT); 
		} catch (CarException e) {
			e.printStackTrace();
		} 
		if(assertEquals(test.speed, 20) && assertEquals(test.steeringDegree, -50)) {
			System.out.println("SUCCESSFUL: Test Collision Avoidance Base Method");
		}
		else {
			System.out.println("FAILED: Test Collision Avoidance Base Method. Expected speed 20 and steering degree -50, got "
							 	+ test.speed + " and " + test.steeringDegree);
		}
	}
	
	// makes no sense to test this as it doesn't change any programme states, only shows warnings to user
//	private void testHandleCarException() {
//		
//	}

	//////////////// system tests ////////////////////////////////////////////////////////////
	
	private void testCollisionAvoidanceFront() {
		this.distanceFrontLeft = 0.5;
		try {
			Thread.sleep(3000); // give it some time to react
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		if(assertEquals(this.speed, 20) && assertEquals(this.steeringDegree, 50)) {
			System.out.println("SUCCESSFUL: Test Collision Avoidance Front");
		}
		else {
			System.out.println("FAILED: Test Collision Avoidance Front. Expected speed 20 and steering degree 50, got " 
								+ this.speed + " and " + this.steeringDegree);
		}
		this.distanceFrontLeft = 2; // reset value, in reality, distance should increase anyways bc we steered away from obstacle
	}

	private void testCollisionAvoidanceBack() {
		this.distanceBackRight = 0;
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(assertEquals(this.speed, 10) && assertEquals(this.steeringDegree, 0)) {
			System.out.println("SUCCESSFUL: Test Collision Avoidance Back");
		}
		else {
			System.out.println("FAILED: Test Collision Avoidance Back. Expected speed 10 and steering degree 0, got " 
								+ this.speed + " and " + this.steeringDegree);
		}
		this.distanceBackRight = 26; // reset value
	}
	
	private boolean assertEquals(int value1, int value2) {
		return value1 == value2;
	}

	@Override
	public double getDistance(Sensor sensor) throws NoDataException {
		switch(sensor) {
		case FRONTLEFT:
			return this.distanceFrontLeft;
		case FRONTRIGHT:
			return this.distanceFrontRight;
		case BACKLEFT:
			return this.distanceBackLeft;
		case BACKRIGHT:
			return this.distanceBackRight;
		default:
			return -1;
		}
	}

	@Override
	public void setSpeed(int x) throws NoReactionException {
		this.speed = x;
	}

	@Override
	public void steering(int x) throws NoReactionException {
		this.steeringDegree = x;	
	}
}
