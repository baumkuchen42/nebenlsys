package ev3;

import base.CarController;
import base.ConsoleOutput;
import base.NoUserOutput;
import ev3.EV3;
import lejos.hardware.Button;

public class EV3EV3 {
	public static void main(String[] args) {
		EV3 ev3 = new EV3();
		final CarController ctrl = new CarController(ev3, ev3, new NoUserOutput());
		new Thread(new Runnable() {
			public void run() {
				ctrl.main();
			}
		}).start();
		Button.waitForAnyPress();
		ctrl.stopProgram();
	}
}
