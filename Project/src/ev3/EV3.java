package ev3;

import base.CarException;
import base.CarMotorOutput;
import base.CarSensorInput;
import base.NoDataException;
import lejos.hardware.motor.NXTRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.hardware.sensor.NXTUltrasonicSensor;

public class EV3 implements CarSensorInput, CarMotorOutput {
	private RegulatedMotor leftMotor;
	private RegulatedMotor rightMotor;
	private RegulatedMotor steeringMotor;
	private EV3Sensor sensorFrontLeft;
	private EV3Sensor sensorFrontRight;
	private EV3Sensor sensorBackLeft;
	private EV3Sensor sensorBackRight;
	private static final double MAX_SPEED = 300;
	private static final double MAX_STEERING_DEGREE = 70;

	public EV3() {
		leftMotor = new NXTRegulatedMotor(MotorPort.A);
		rightMotor = new NXTRegulatedMotor(MotorPort.C);
		leftMotor.setSpeed(0);
		rightMotor.setSpeed(0);
		steeringMotor = new NXTRegulatedMotor(MotorPort.B);
		sensorFrontLeft = new EV3Sensor(SensorPort.S1);
		sensorFrontRight = new EV3Sensor(SensorPort.S4);
		sensorBackLeft = new EV3Sensor(SensorPort.S3);
		sensorBackRight = new EV3Sensor(SensorPort.S2);
	}
	
	@Override
	public void setSpeed(int x) throws CarException {
		double newSpeed = (double)x/100 * MAX_SPEED;
		int oldSpeed = leftMotor.getSpeed();
		if(x >= 0) {
			leftMotor.setSpeed((int) newSpeed);
			rightMotor.setSpeed((int) newSpeed);
			leftMotor.backward(); // apparently the motors are wired oppositely
			rightMotor.backward();
			
		}
		else if(x < 0) {
			leftMotor.setSpeed(-(int) newSpeed);
			rightMotor.setSpeed(-(int) newSpeed);
			leftMotor.forward();
			rightMotor.forward();
		}
	}

	@Override
	public void steering(int x) throws CarException {
		double degree = (double)x/100 * MAX_STEERING_DEGREE;
		steeringMotor.rotateTo((int)degree);
//		if(-100 <= x && x < 0) {
//			turnLeft();
//		}
//		else if(x == 0) {
//			steeringMotor.rotateTo(0);
//		}
//		else if(0 < x && x <= 100) {
//			turnRight();
//		}
	}

	private void turnLeft() {
		steeringMotor.rotateTo(-70);
	}
	
	private void turnRight() {
		steeringMotor.rotateTo(70);
	}

	@Override
	public double getDistance(Sensor sensor) throws CarException {
		switch(sensor) {
			case FRONTLEFT:
				return sensorFrontLeft.measureDistance();
			case FRONTRIGHT:
				return sensorFrontRight.measureDistance();
			case BACKLEFT:
				return sensorBackLeft.measureDistance();
			case BACKRIGHT:
				return sensorBackRight.measureDistance();
			default:
				throw new NoDataException();
		}
	}
	
	private class EV3Sensor{
		private NXTUltrasonicSensor mySensor;
		private SampleProvider sampleProvider;
		public EV3Sensor(Port port) {
			 this.mySensor = new NXTUltrasonicSensor(port);
			 this.mySensor.enable();
			 this.sampleProvider = this.mySensor.getDistanceMode();
		}
		
		protected double measureDistance() throws NoDataException{
			float samples[] = new float[sampleProvider.sampleSize()];
			try {
				sampleProvider.fetchSample(samples, 0);
			}
			catch(Exception ex) {
				throw new NoDataException();
			}
			if(samples.length == 0) {
				throw new NoDataException();
			}
//			else if(Double.isInfinite(samples[0]))
//				throw new NoDataException();
			return samples[0] * 100;
		}
	}
}
