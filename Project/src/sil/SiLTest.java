package sil;

import base.CarMotorOutput;
import base.CarSensorInput;
import base.NoDataException;
import base.NoReactionException;

public class SiLTest implements CarSensorInput, CarMotorOutput{
	private int speed = -1;
	private int steeringDegree = -1;
	private double distanceFrontLeft = 2;
	private double distanceFrontRight = 13;
	private double distanceBackLeft = 42;
	private double distanceBackRight = 26;
	
	public void main() {
		testCollisionAvoidanceFront();
		testCollisionAvoidanceBack();
		System.exit(0);
	}
	
	//////// system tests ////////////////////////////////////////////////
	
	private void testCollisionAvoidanceFront() {
		try {
			approachObstacle(true);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(assertEquals(this.speed, 20) && assertEquals(this.steeringDegree, 50)) {
			System.out.println("SUCCESSFUL: Test Collision Avoidance Front");
		}
		else {
			System.out.println("FAILED: Test Collision Avoidance Front. Expected speed 20 and steering degree 50, got " 
								+ this.speed + " and " + this.steeringDegree);
		}
		try {
			moveAwayFromObstacle(true);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}

	private void approachObstacle(boolean front) throws InterruptedException {
		if(front) {
			for(double i=0.5; i>=0.25; i-=0.05) {
				this.distanceFrontLeft = i;
				Thread.sleep(100);
			}
		}
		else {
			for(double i=0.5; i>=0.25; i-=0.05) {
				this.distanceBackRight = i;
				Thread.sleep(100);
			}
		}
	}
	
	private void moveAwayFromObstacle(boolean front) throws InterruptedException {
		if(front) {
			for(double i=0.25; i<=2; i+=0.5) {
				this.distanceFrontLeft = i;
				Thread.sleep(100);
			}
		}
		else {
			for(double i=0.25; i<=2; i+=0.5) {
				this.distanceBackRight = i;
				Thread.sleep(100);
			}
		}
	}

	private void testCollisionAvoidanceBack() {
		try {
			approachObstacle(false);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(assertEquals(this.speed, 10) && assertEquals(this.steeringDegree, 0)) {
			System.out.println("SUCCESSFUL: Test Collision Avoidance Back");
		}
		else {
			System.out.println("FAILED: Test Collision Avoidance Back. Expected speed 10 and steering degree 0, got " 
								+ this.speed + " and " + this.steeringDegree);
		}
		try {
			moveAwayFromObstacle(false);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private boolean assertEquals(int value1, int value2) {
		return value1 == value2;
	}
	
	
	////// environment simulation //////////////////////////////////////////////////
	
	@Override
	public double getDistance(Sensor sensor) throws NoDataException {
		switch(sensor) {
		case FRONTLEFT:
			return this.distanceFrontLeft;
		case FRONTRIGHT:
			return this.distanceFrontRight;
		case BACKLEFT:
			return this.distanceBackLeft;
		case BACKRIGHT:
			return this.distanceBackRight;
		default:
			return -1;
		}
	}

	@Override
	public void setSpeed(int x) throws NoReactionException {
		this.speed = x;
	}

	@Override
	public void steering(int x) throws NoReactionException {
		this.steeringDegree = x;	
	}
}
