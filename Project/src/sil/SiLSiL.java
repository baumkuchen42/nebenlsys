package sil;

import base.CarController;
import base.ConsoleOutput;

public class SiLSiL {
	public static void main(String[] args) {
		SiLTest sil = new SiLTest();
		CarController ctrl = new CarController(sil, sil, new ConsoleOutput());
		ctrl.main();
		sil.main();
	}
}
