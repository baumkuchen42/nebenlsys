package nxt;

import base.CarException;
import base.CarMotorOutput;
import base.CarSensorInput;
import base.NoDataException;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.I2CPort;

public class NXT implements CarSensorInput, CarMotorOutput {
	private NXTRegulatedMotor leftMotor;
	private NXTRegulatedMotor rightMotor;
	private NXTRegulatedMotor steeringMotor;
	private NXTSensor sensorFrontLeft;
	private NXTSensor sensorFrontRight;
	private NXTSensor sensorBackLeft;
	private NXTSensor sensorBackRight;
	private final int MAX_SPEED = 300;
	
	public NXT() {
		leftMotor = new NXTRegulatedMotor(MotorPort.A);
		rightMotor = new NXTRegulatedMotor(MotorPort.C);
		leftMotor.setSpeed(0);
		rightMotor.setSpeed(0);
		steeringMotor = new NXTRegulatedMotor(MotorPort.B);
		sensorFrontLeft = new NXTSensor(SensorPort.S1); 
		sensorFrontRight = new NXTSensor(SensorPort.S4);
		sensorBackLeft = new NXTSensor(SensorPort.S3);
		sensorBackRight = new NXTSensor(SensorPort.S2);
	}

	@Override
	public void setSpeed(int x) throws CarException {
		if(x >= 0) {
			int oldSpeed = leftMotor.getSpeed();
			leftMotor.setSpeed(x/100 * MAX_SPEED);
			rightMotor.setSpeed(x/100 * MAX_SPEED);
			if(oldSpeed == 0) {
				leftMotor.forward();
				rightMotor.forward();
			}
		}
		else if(x < 0) {

			leftMotor.setSpeed(x/100 * MAX_SPEED);
			rightMotor.setSpeed(x/100 * MAX_SPEED);
			leftMotor.backward();
			rightMotor.backward();
		}
	}

	@Override
	public void steering(int x) throws CarException {
		if(-100 <= x && x < 0) {
			turnLeft();
		}
		else if(x == 0) {
			steeringMotor.rotateTo(0);
		}
		else if(0 < x && x <= 100) {
			turnRight();
		}
	}

	private void turnLeft() {
		steeringMotor.rotateTo(-45);
	}
	
	private void turnRight() {
		steeringMotor.rotateTo(45);
	}


	@Override
	public double getDistance(Sensor sensor) throws CarException {
		switch(sensor) {
		case FRONTLEFT:
			return sensorFrontLeft.measureDistance();
		case FRONTRIGHT:
			return sensorFrontRight.measureDistance();
		case BACKLEFT:
			return sensorBackLeft.measureDistance();
		case BACKRIGHT:
			return sensorBackRight.measureDistance();
		default:
			throw new NoDataException();
		}
	}
	
	private class NXTSensor extends UltrasonicSensor{
		public NXTSensor(I2CPort port) {
			 super(port);
			 this.capture();
		}
		
		protected double measureDistance() throws NoDataException{
			return this.getDistance();
		}
	}

}
