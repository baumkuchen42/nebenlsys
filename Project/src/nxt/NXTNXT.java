package nxt;

import nxt.NXT;
import base.CarController;
import base.ConsoleOutput;
import lejos.nxt.Button;

public class NXTNXT {
	public static void main(String[] args) {
		NXT nxt = new NXT();
		CarController ctrl = new CarController(nxt, nxt, new ConsoleOutput());
		new Thread(new Runnable() {
			public void run() {
				ctrl.main();
			}
		}).start();
		Button.waitForAnyPress();
		ctrl.stopProgram();
	}
}
