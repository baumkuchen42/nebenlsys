package sil_gui;

import base.CarController;
import sil.SiLTest;
import gui.GUI;

public class SiLGUI {
	public static void main(String[] args) {
		GUI gui = new GUI();
		CarController ctrl = new CarController(new SiLTest(), gui, gui);
		ctrl.main();
	}
}
