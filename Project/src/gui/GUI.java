package gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;

import base.CarException;
import base.CarMotorOutput;
import base.CarSensorInput;
import base.NoReactionException;
import base.UserNotifyOutput;

public class GUI implements CarMotorOutput, CarSensorInput, UserNotifyOutput{
	private int speed = 0;
	private int steeringDegree = 0;
	private double distanceFrontLeft = 50;
	private double distanceFrontRight = 50;
	private double distanceBackLeft = 50;
	private double distanceBackRight = 50;
	
	private JPanel leftPanel;
	private JPanel middlePanel;
	private JPanel rightPanel;
    
    public GUI() {
    	buildWindow();
    }
    
	private void buildWindow() {
		JFrame frame = new JFrame("CarControllerGUI");
		frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
            	System.exit(0);
            }
        });
		
		frame.setLayout(new FlowLayout());
		frame.setSize(900,300);
		leftPanel = buildLeftPanel();
		middlePanel = buildMiddlePanel();
		rightPanel = buildRightPanel();
		frame.getContentPane().add(leftPanel);
		frame.getContentPane().add(middlePanel);
		frame.getContentPane().add(rightPanel);
		frame.pack();
		frame.setVisible(true);
	}
	
	private JPanel buildLeftPanel() {
		JPanel panel = new JPanel(new GridLayout(4, 2));
		createTitle(panel, "Environment Ctrls");
		panel.add(new JLabel("FLSensor"));
		panel.add(buildSlider(Sensor.FRONTLEFT));
		panel.add(new JLabel("FRSensor"));
		panel.add(buildSlider(Sensor.FRONTRIGHT));
		panel.add(new JLabel("BLSensor"));
		panel.add(buildSlider(Sensor.BACKLEFT));
		panel.add(new JLabel("BRSensor"));
		panel.add(buildSlider(Sensor.BACKRIGHT));
		panel.setVisible(true);
		return panel;
	}
	
	private JPanel buildMiddlePanel() {
		JPanel panel = new JPanel(new GridLayout(2, 2));
		createTitle(panel, "Car Events");
		panel.add(new JLabel("Direction: "));
		panel.add(createNamedLabel("directionView", Integer.toString(steeringDegree)));
		panel.add(new JLabel("Speed: "));
		panel.add(createNamedLabel("speedView", Integer.toString(speed)));
		panel.setVisible(true);
		return panel;
	}
	
	private JPanel buildRightPanel() {
		JPanel panel = new JPanel(new GridLayout(4, 2));
		createTitle(panel, "Sensory Output");
		panel.add(new JLabel("FLSensor"));
		panel.add(createNamedLabel("FLSensorValue", Double.toString(distanceFrontLeft)));
		panel.add(new JLabel("FRSensor"));
		panel.add(createNamedLabel("FRSensorValue", Double.toString(distanceFrontRight)));
		panel.add(new JLabel("BLSensor"));
		panel.add(createNamedLabel("BLSensorValue", Double.toString(distanceBackLeft)));
		panel.add(new JLabel("BRSensor"));
		panel.add(createNamedLabel("BRSensorValue", Double.toString(distanceBackRight)));
		panel.setVisible(true);
		return panel;
	}
	
	private JLabel createNamedLabel(String name, String content) {
		JLabel label = new JLabel(content);
		label.setName(name);
		return label;
	}
	
	private void createTitle(JPanel panel, String title) {
		TitledBorder border = new TitledBorder(title);
	    border.setTitleJustification(TitledBorder.CENTER);
	    border.setTitlePosition(TitledBorder.BOTTOM);
	    panel.setBorder(border);
	}
	
	private JSlider buildSlider(Sensor sensor) {
		double distance = -1;
		switch(sensor) {
		case FRONTRIGHT:
			distance = this.distanceFrontRight;
			break;
		case FRONTLEFT:
			distance = this.distanceFrontLeft;
			break;
		case BACKRIGHT:
			distance = this.distanceBackRight;
			break;
		case BACKLEFT:
			distance = this.distanceBackLeft;
			break;
		}
		JSlider slider = new JSlider(0, 100, (int) distance);
		slider.addChangeListener(event -> onSliderChange(event, sensor));
		return slider;
	}
	
	private void onSliderChange(ChangeEvent event, Sensor sensor) {
		double value = ((JSlider) event.getSource()).getValue();
		switch(sensor) {
		case FRONTLEFT:
			distanceFrontLeft = value;
			break;
		case FRONTRIGHT:
			distanceFrontRight = value;
			break;
		case BACKLEFT:
			distanceBackLeft = value;
			break;
		case BACKRIGHT:
			distanceBackRight = value;
			break;
		}
	}
	
	private Component getComponentByName(Container container, String name) {
		for(Component comp : container.getComponents()) {
			if(comp.getName() == name) {
				return comp;
			}
		}
		return null;
	}

	@Override
	public double getDistance(Sensor sensor) throws CarException {
		switch(sensor) {
		case FRONTLEFT:
			return this.distanceFrontLeft;
		case FRONTRIGHT:
			return this.distanceFrontRight;
		case BACKLEFT:
			return this.distanceBackLeft;
		case BACKRIGHT:
			return this.distanceBackRight;
		default:
			return -1;
		}
	}

	@Override
	public void setSpeed(int x) throws NoReactionException {
		this.speed = x;
		((JLabel) getComponentByName(middlePanel, "speedView")).setText(Integer.toString(x));
	}

	@Override
	public void steering(int x) throws NoReactionException {
		this.steeringDegree = x;
		if(-100 <= x && x < 0) {
			((JLabel) getComponentByName(middlePanel, "directionView")).setText("left");
		}
		else if(x == 0) {
			((JLabel) getComponentByName(middlePanel, "directionView")).setText("straight");
		}
		else if(0 < x && x <= 100) {
			((JLabel) getComponentByName(middlePanel, "directionView")).setText("right");
		}
		else {
			((JLabel) getComponentByName(middlePanel, "directionView")).setText("confused car");
		}
	}

	@Override
	public void refresh_value(Sensor sensor, double value) {
		switch(sensor) {
		case FRONTLEFT:
			((JLabel) getComponentByName(rightPanel, "FLSensorValue")).setText(Double.toString(value));
			break;
		case FRONTRIGHT:
			((JLabel) getComponentByName(rightPanel, "FRSensorValue")).setText(Double.toString(value));
			break;
		case BACKLEFT:
			((JLabel) getComponentByName(rightPanel, "BLSensorValue")).setText(Double.toString(value));
			break;
		case BACKRIGHT:
			((JLabel) getComponentByName(rightPanel, "BRSensorValue")).setText(Double.toString(value));
			break;
		}
	}
	
	@Override
	public void showNoDataWarning() {
		JOptionPane msg = new JOptionPane();
		msg.showMessageDialog(rightPanel, "No value could be retrieved.");
		msg.setVisible(true);
	}
	
	@Override
	public void showNoReactionWarning() {
		JOptionPane msg = new JOptionPane();
		msg.showMessageDialog(middlePanel, "CAUTION! Car did not react to command!");
	}
}
