package ev3;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.robotics.RegulatedMotor;
import lejos.hardware.sensor.EV3UltrasonicSensor;

public class EV3 implements CarSensorInput, CarMotorOutput {
	private RegulatedMotor leftMotor;
	private RegulatedMotor rightMotor;
	private EV3Sensor sensorFrontLeft;
	private EV3Sensor sensorFrontRight;
	private EV3Sensor sensorBackLeft;
	private EV3Sensor sensorBackRight;
	private final int MAX_SPEED = 300;

	public EV3() {
		leftMotor = new EV3LargeRegulatedMotor(MotorPort.A);
		rightMotor = new EV3LargeRegulatedMotor(MotorPort.B);
		sensorFrontLeft = new EV3Sensor(SensorPort.S1);
		sensorFrontRight = new EV3Sensor(SensorPort.S4);
		sensorBackLeft = new EV3Sensor(SensorPort.S3);
		sensorBackRight = new EV3Sensor(SensorPort.S2);
	}
	
	@Override
	public void setSpeed(int x) throws CarException {
		if(x >= 0) {
			leftMotor.setSpeed(x/100 * MAX_SPEED);
			rightMotor.setSpeed(x/100 * MAX_SPEED);
			leftMotor.forward();
			rightMotor.forward();
		}
		else if(x < 0) {

			leftMotor.setSpeed(x/100 * MAX_SPEED);
			rightMotor.setSpeed(x/100 * MAX_SPEED);
			leftMotor.backward();
			rightMotor.backward();
		}
	}

	@Override
	public void steering(int x) throws CarException {
		if(-100 <= x && x < 0) {
			turnLeft(400);
		}
		else if(x == 0) {
			leftMotor.rotateTo(0);
			rightMotor.rotateTo(0);
		}
		else if(0 < x && x <= 100) {
			turnRight(400);
		}
	}

	private void turnLeft(int speed) {
		leftMotor.rotate(speed, true);
		rightMotor.rotate((int)Math.round(speed/40));
	}
	
	private void turnRight(int speed) {
		rightMotor.rotate(speed, true);
		leftMotor.rotate((int)Math.round(speed/40));
	}

	@Override
	public double getDistance(Sensor sensor) throws CarException {
		switch(sensor) {
			case FRONTLEFT:
				return sensorFrontLeft.measureDistance();
			case FRONTRIGHT:
				return sensorFrontRight.measureDistance();
			case BACKLEFT:
				return sensorBackLeft.measureDistance();
			case BACKRIGHT:
				return sensorBackRight.measureDistance();
			default:
				throw new NoDataException();
		}
	}
	
	private class EV3Sensor extends EV3UltrasonicSensor{
		public EV3Sensor(Port port) {
			 super(port);
			 this.getMode("Distance");
		}
		
		protected double measureDistance() throws NoDataException{
			float samples[] = new float[this.sampleSize()];
			this.fetchSample(samples, 0);
			if(samples.length == 0) {
				throw new NoDataException();
			}
			return samples[0];
		}
	}
}
