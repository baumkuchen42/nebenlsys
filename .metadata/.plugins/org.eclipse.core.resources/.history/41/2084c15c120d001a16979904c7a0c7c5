package base;

import base.CarSensorInput.Sensor;

/* the car */
public class CarController {
	private CarSensorInput sensorInputListener;
	private CarMotorOutput motor;
	private static double MIN_DIST = 1; //minimal distance to closest object
	private boolean running = true;
	private UserNotifyOutput notifyOutput;
	
	/*
	 * asynchronous query of sensory input (via 4 threads for each sensor) and reacting on values 
	 * (e.g. stop when value, which means distance to next object, is 0)
	 * also print values in terminal if they've changed
	 */
	CarController(CarSensorInput sensorInputListener, CarMotorOutput motor, UserNotifyOutput notifyOutput) {
		this.sensorInputListener = sensorInputListener;
		this.motor = motor;
		this.notifyOutput = notifyOutput;
	}
	
	public void main() {
		if(isAllClear()) {
			try {
				driveForwards();
			} catch (CarException e) {
				handleCarException(e);
			}
			//start threads for monitoring sensory input and reacting to it
			checkSensorFrontLeft();
			checkSensorFrontRight();
			checkSensorBackLeft();
			checkSensorBackRight();
		}
	}
	
	void checkSensorFrontLeft() {
		new Thread(new Runnable() { 
            public void run() { 
            	checkSensor(Sensor.FRONTLEFT);
            } 
        }).start(); 
	}
	
	void checkSensorFrontRight() {
		new Thread(new Runnable() { 
            public void run() { 
            	checkSensor(Sensor.FRONTRIGHT);
            } 
        }).start(); 
	}
	
	void checkSensorBackLeft() {
		new Thread(new Runnable() { 
            public void run() { 
            	checkSensor(Sensor.BACKLEFT);
            } 
        }).start(); 
	}
	
	void checkSensorBackRight() {
		new Thread(new Runnable() { 
            public void run() { 
            	checkSensor(Sensor.BACKRIGHT);
            } 
        }).start(); 
	}
	
	void checkSensor(Sensor sensor) {
		double distance = -1; //set default value of distance
		
		if(sensorInputListener != null) {
			while(this.running) {
				try {
					if(distance == -1) { //if initial value not retrieved yet
						distance = sensorInputListener.getDistance(sensor); //retrieve initial value
						refresh_value(sensor, distance);
					}
					double new_distance = sensorInputListener.getDistance(sensor);
					
					if(new_distance < MIN_DIST) { //if distance smaller than given minimum distance
						new Thread(new Runnable() { //start collision avoidance in new thread
							public void run() {
								try {
									collisionAvoidance(sensor);
								} catch (CarException e) {
									handleCarException(e);
								}
							}
						}).start();
					}
					else if(isAllClear()) {
						driveForwards();
					}
					
					if(new_distance != distance) { //if new value is different from old
						refresh_value(sensor, new_distance); //show the user
						distance = new_distance;
					}
					
					Thread.sleep(100); // wait a little bit before fetching next value for given sensor so that each sensoring thread gets a turn regularly
					
				} catch (CarException e) {
					handleCarException(e);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void refresh_value(Sensor sensor, double value) {
		notifyOutput.refresh_value(sensor, value);
	}
	
	private void handleCarException(CarException e) {
		if(e instanceof NoDataException) {
			showNoDataWarning();
		}
		else if(e instanceof NoReactionException) {
			initiateNoReactionEmergency();
		}
	}
	
	protected synchronized void driveForwards() throws CarException {
		motor.steering(0);
		motor.setSpeed(50);
	}
	
	protected synchronized void collisionAvoidance (Sensor sensor) throws CarException{
		switch(sensor) {
			case FRONTLEFT:
				motor.setSpeed(20); //slow down
				motor.steering(50); //steer to the right
				break;
			case FRONTRIGHT:
				motor.setSpeed(20); 
				motor.steering(-50); //steer to the left
				break;
			case BACKLEFT: //assume that car is driving backwards, otherwise obstacle in back wouldn't be a problem
				motor.setSpeed(10); //slowly drive forwards
				motor.steering(0); //forwards
				break;
			case BACKRIGHT:
				motor.setSpeed(10);
				motor.steering(0);
				break;
		}
	}
	
	private boolean isAllClear() {
		try {
			return sensorInputListener.getDistance(Sensor.FRONTLEFT) > MIN_DIST
					&& sensorInputListener.getDistance(Sensor.FRONTRIGHT) > MIN_DIST
					&& sensorInputListener.getDistance(Sensor.BACKLEFT) > MIN_DIST
					&& sensorInputListener.getDistance(Sensor.BACKRIGHT) > MIN_DIST;
		} catch (CarException e) {
			showNoDataWarning();
			return false;
		}
	}
	
	private void showNoDataWarning() {
		notifyOutput.showNoDataWarning();
	}
	
	private void initiateNoReactionEmergency() {
		notifyOutput.showNoReactionWarning();
	}
	
	void stopProgram() {
		System.out.println("Program terminated");
		this.running = false;
	}
}
