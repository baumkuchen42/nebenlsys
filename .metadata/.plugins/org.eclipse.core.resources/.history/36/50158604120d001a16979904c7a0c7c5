package nxt;

import lejos.nxt.MotorPort;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.I2CPort;

public class NXT implements CarSensorInput, CarMotorOutput {
	private NXTRegulatedMotor leftMotor;
	private NXTRegulatedMotor rightMotor;
	private NXTSensor sensorFrontLeft;
	private NXTSensor sensorFrontRight;
	private NXTSensor sensorBackLeft;
	private NXTSensor sensorBackRight;
	private final int MAX_SPEED = 300;
	
	public NXT() {
		leftMotor = new NXTRegulatedMotor(MotorPort.A); // or whatever port the left motor is on
		rightMotor = new NXTRegulatedMotor(MotorPort.B); // see above
		sensorFrontLeft = new NXTSensor(SensorPort.S1); 
		sensorFrontRight = new NXTSensor(SensorPort.S4);
		sensorBackLeft = new NXTSensor(SensorPort.S3);
		sensorBackRight = new NXTSensor(SensorPort.S2);
	}

	@Override
	public void setSpeed(int x) throws CarException {
		if(x >= 0) {
			leftMotor.setSpeed(x/100 * MAX_SPEED);
			rightMotor.setSpeed(x/100 * MAX_SPEED);
			leftMotor.forward();
			rightMotor.forward();
		}
		else if(x < 0) {

			leftMotor.setSpeed(x/100 * MAX_SPEED);
			rightMotor.setSpeed(x/100 * MAX_SPEED);
			leftMotor.backward();
			rightMotor.backward();
		}
	}

	@Override
	public void steering(int x) throws CarException {
		if(-100 <= x && x < 0) {
			turnLeft(400);
		}
		else if(x == 0) {
			leftMotor.rotateTo(0);
			rightMotor.rotateTo(0);
		}
		else if(0 < x && x <= 100) {
			turnRight(400);
		}
	}

	private void turnLeft(int speed) {
		leftMotor.rotate(speed, true);
		rightMotor.rotate((int)Math.round(speed/40));
	}
	
	private void turnRight(int speed) {
		rightMotor.rotate(speed, true);
		leftMotor.rotate((int)Math.round(speed/40));
	}


	@Override
	public double getDistance(Sensor sensor) throws CarException {
		switch(sensor) {
		case FRONTLEFT:
			return sensorFrontLeft.measureDistance();
		case FRONTRIGHT:
			return sensorFrontRight.measureDistance();
		case BACKLEFT:
			return sensorBackLeft.measureDistance();
		case BACKRIGHT:
			return sensorBackRight.measureDistance();
		default:
			throw new NoDataException();
		}
	}
	
	private class NXTSensor extends UltrasonicSensor{
		public NXTSensor(I2CPort port) {
			 super(port);
			 this.capture();
		}
		
		protected double measureDistance() throws NoDataException{
			return this.getDistance();
		}
	}

}
