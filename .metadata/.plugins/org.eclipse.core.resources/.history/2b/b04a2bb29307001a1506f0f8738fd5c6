package task1;

import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.robotics.RegulatedMotor;
import lejos.hardware.sensor.EV3UltrasonicSensor;

public class EV3 implements CarSensorInput, CarMotorOutput {
	private RegulatedMotor leftMotor;
	private RegulatedMotor rightMotor;
	private EV3UltrasonicSensor sensorFrontLeft;
	private EV3UltrasonicSensor sensorFrontRight;
	private EV3UltrasonicSensor sensorBackLeft;
	private EV3UltrasonicSensor sensorBackRight;

	public EV3() {
		leftMotor = new EV3LargeRegulatedMotor(MotorPort.A);
		rightMotor = new EV3LargeRegulatedMotor(MotorPort.B);
		sensorFrontLeft = new EV3UltrasonicSensor(SensorPort.S1);
		sensorFrontLeft.getMode("Distance");
		sensorFrontRight = new EV3UltrasonicSensor(SensorPort.S4);
		sensorFrontRight.getMode("Distance");
		sensorBackLeft = new EV3UltrasonicSensor(SensorPort.S3);
		sensorBackLeft.getMode("Distance");
		sensorBackRight = new EV3UltrasonicSensor(SensorPort.S2);
		sensorBackRight.getMode("Distance");
	}
	
	@Override
	public void setSpeed(int x) throws CarException {
		leftMotor.setSpeed(x);
		rightMotor.setSpeed(x);
	}

	@Override
	public void steering(int x) throws CarException {
		if(-100 <= x && x < 0) {
			turnLeft(400);
		}
		else if(x == 0) {
			leftMotor.forward();
			rightMotor.forward();
		}
		else if(0 < x && x <= 100) {
			turnRight(400);
		}
	}

	private void turnLeft(int speed) {
		leftMotor.rotate(speed, true);
		rightMotor.rotate((int)Math.round(speed/40));
	}
	
	private void turnRight(int speed) {
		rightMotor.rotate(speed, true);
		leftMotor.rotate((int)Math.round(speed/40));
	}

	@Override
	public double getDistance(Sensor sensor) throws CarException {
		switch(sensor) {
			case FRONTLEFT:
				return measureDistance(sensorFrontLeft);
			case FRONTRIGHT:
				return measureDistance(sensorFrontRight);
			case BACKLEFT:
				return measureDistance(sensorBackLeft);
			case BACKRIGHT:
				return measureDistance(sensorBackRight);
			default:
				return -1;
		}
	}

	private double measureDistance(EV3UltrasonicSensor sensor) {
		float samples[] = new float[sensor.sampleSize()];
		sensor.fetchSample(samples, 0);
		return samples[0];
	}
}
